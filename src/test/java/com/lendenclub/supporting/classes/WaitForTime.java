package com.lendenclub.supporting.classes;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lendenclub.test.main.BorrowerRegistration;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public class WaitForTime {
	
	public void pageWaitForTime(int time)
	{
//		int time =0;
//		String id = "613f36402";
		try {
			WebDriverWait wait = new WebDriverWait(BorrowerRegistration.driver, time);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
	}
	
	public void pageWaitForTimeAndInspectElement(int time, String id)
	{
//		int time =0;
//		String id = "613f36402";
		try {
			WebDriverWait wait = new WebDriverWait(BorrowerRegistration.driver, time);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(id)));
		} catch (Exception ex) {
			ex.getMessage();
		}
	}
	
	public void pageScroll(String text)
	{
//		\"Allow\"))
//		 Scroll down with the help of scroll bar till Allow text
		MobileElement element = (MobileElement) BorrowerRegistration.driver.findElement(
				MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).setAsVerticalList()"
						+ ".scrollIntoView(new UiSelector().text(\""+text+"\"))"));
	
	}
}
