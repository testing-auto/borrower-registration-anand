package com.lendenclub.test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class UploadEnach {
	WaitForTime time = new WaitForTime();

	public void page009Enach() {
		
		WebDriverWait waiting = new WebDriverWait(BorrowerRegistration.driver, 8);
		WebElement element1 = waiting.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='SUBMIT']")));
		element1.click();
		try { 
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).click();
		} catch(Exception e)  { }

		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='PROCEED TO SIGN eNACH']"))
		.click();
		} catch(Exception e) { }
		time.pageWaitForTime(2);
		((AndroidDriver<MobileElement>) BorrowerRegistration.driver).activateApp("com.innofinsolutions.instamoney");
		BorrowerRegistration.driver.terminateApp("com.android.chrome");
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Click physical natch
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SIGN PHYSICAL MANDATE']"))
		.click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Download Natch Form
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Download']")).click();
		// BorrowerRegistration.driver.manage().timeouts().implicitlyWait(3,
		// TimeUnit.SECONDS);

		try {
			WebDriverWait wait = new WebDriverWait(BorrowerRegistration.driver, 4);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402cf46")));
		} catch (org.openqa.selenium.TimeoutException ex) {
			ex.getMessage();
		}
		((AndroidDriver<MobileElement>) BorrowerRegistration.driver).activateApp("com.innofinsolutions.instamoney");
		BorrowerRegistration.driver.terminateApp("com.android.chrome");
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]"))
			.click(); // Normal click on screen
		} catch (Exception e) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click();
		} catch (Exception e) {
		}
		time.pageWaitForTime(4);
		// upload signed update
		try {
			BorrowerRegistration.driver
			.findElement(By.xpath("//android.widget.TextView[@text='Upload The Signed Mandate']")).click();
		} catch (Exception ex) {
			System.out.println("Message is : " + ex.getMessage());
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]"))
			.click();
		} catch (Exception ex) {
			System.out.println("Xpath message is : " + ex.getCause());
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Select photos from galary
		BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click();
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
		} catch (Exception e) {
		}
		try {
			WebDriverWait wait = new WebDriverWait(BorrowerRegistration.driver, 1);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView")));
			element.click();
			// BorrowerRegistration.driver.findElement(By.xpath(
			// "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.ImageView"))
			// .click(); // Photos
			// BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5,
			// TimeUnit.SECONDS);
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']"))
			.click(); // Aadhar Card
			BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			BorrowerRegistration.driver
			.findElement(By.xpath("//com.oplus.gallery.business_lib.ui.view.SlotView[@index='1']")).click(); // pic
			// time.pageWaitForTime(5);

		} catch (Exception e) {
		}
		// time.pageWaitForTime(5);
		time.pageWaitForTime(12);
		time.pageScroll("CONTINUE");
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='CONTINUE']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			BorrowerRegistration.driver
			.findElement(
					By.xpath("com.android.permissioncontroller:id/permission_allow_foreground_only_button"))
			.click();
		} catch (Exception ex) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='WHILE USING THE APP']"))
			.click();
		} catch (Exception e) {
		}

		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@index='1']")).click();
		} catch (Exception e) {
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Save & Proceed']")).click();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		time.pageWaitForTime(4);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SAVE & CONTINUE']")).click();
		time.pageWaitForTime(4);
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='check
		// status']")).click();
		// time.pageWaitForTime(4);
		// try {
		// BorrowerRegistration.driver.findElement(By.xpath(
		// "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[6]/android.widget.ImageView"))
		// .click(); // Select Rating
		// } catch (Exception e) {
		// }
		//
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT
		// RATING']")).click();
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Fast
		// Process']")).click(); // Thank
		// // u
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SAVE
		// FEEDBACK']")).click();
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='NO']")).click();
		// BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='GO
		// TO DASHBOARD']")).click();
		// time.pageWaitForTime(3);
	}
}
