package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class AssessmentFee {
	WaitForTime time = new WaitForTime();

	public void page005AssessmentFee() {
		time.pageScroll("Assessment Fee"); // Scroll bar to scroll till Assessment Fee
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		time.pageWaitForTime(1);
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"))
		.click(); // Click Proceed to pay rs 199
		time.pageWaitForTime(2);
		// Enter name on Registration page
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 13));
		BorrowerRegistration.driver.hideKeyboard();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='Next']")).click(); // Click Next button
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Select Credit Card
		BorrowerRegistration.driver.findElement(By.xpath("//android.view.View[@content-desc=\"Credit Card\"]")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='card-number']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 20)); // Enter Credit Card
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='card-number']")).click();
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Credit Card']")).click();
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Credit Card']")).click();
		// enter expiry month
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='card-expiry-month']")).click();
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.TAB));
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.TAB));
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.DIGIT_0));
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		BorrowerRegistration.driver.hideKeyboard();
		// enter expiry year
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='card-expiry-year']")).click();
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.DIGIT_2));
		BorrowerRegistration.driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		BorrowerRegistration.driver.hideKeyboard();
		// enter cvv
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='card-cvv']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 14));
		BorrowerRegistration.driver.hideKeyboard();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		// click on pay
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@resource-id='submit-form-button']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		// Enter card password
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@resource-id='txtPassword']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 15));
		// click on submit button
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click();
		time.pageWaitForTime(3);
	}

}
