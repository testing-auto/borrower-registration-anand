package com.lendenclub.test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class LoanRequirement {
	WaitForTime time = new WaitForTime();

public void page004LoanRequirement() {
		
		WebElement seek_bar = BorrowerRegistration.driver.findElement(By.xpath("//android.view.ViewGroup[@index='2']"));
		int start = seek_bar.getLocation().getX();
		int end = seek_bar.getSize().getWidth();
		int y = seek_bar.getLocation().getY();
		TouchAction action = new TouchAction(BorrowerRegistration.driver);
		int moveTo = (int) (end * 0.8);
		action.press(PointOption.point(start, y)).moveTo(PointOption.point(moveTo, y)).release().perform();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='4 Months']")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Click Purpose of loan drop down
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Purpose of Loan']")).click();	
		// Select purpose of loan from drop down list
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='Education']")).click();		
		// Click Save load requirement button
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SAVE LOAN REQUIREMENT']")).click();
		time.pageWaitForTime(1);
	}
}
