package com.lendenclub.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class AdharCard {
	WaitForTime time = new WaitForTime();

	public void page007AdharCard() {

		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.TextView"))
				.click(); // Click Front image of Aadhar Card to upload
		BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click(); // Select Gallery option
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Click Personal mobile gallery
		try {
//			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
			System.out.println("Please select details manually");
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView"))
					.click(); // Photos
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']"))
					.click(); // Aadhar Card
			BorrowerRegistration.driver.findElement(By.xpath("//com.oplus.gallery.business_lib.ui.view.SlotView[@index='1']")).click();// Front

		} catch (Exception ex) {
		}
		time.pageWaitForTime(8);
		
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]"))
				.click(); // Select Back side image of adhaar card
		BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click(); // Select Gallery option
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Click Personal mobile gallery
		try {
//			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
//			BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(BorrowerRegistration.driver, 2);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView")));
			element.click();
//			BorrowerRegistration.driver.findElement(By.xpath(
//					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView"))
//					.click(); // Photos
//			BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']"))
					.click(); // Aadhar Card
			BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			BorrowerRegistration.driver.findElement(By.xpath("//com.oplus.gallery.business_lib.ui.view.SlotView[@index='0']")).click();// Back
		} catch (Exception e) {
		}
		time.pageWaitForTime(12);
		time.pageScroll("SUBMIT"); // Scroll bar to scroll till Submit button
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Eg. Mumbai']"))
					.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 16));
		} catch (Exception e) {
		}
		// For hide keyboard
		// BorrowerRegistration.driver.hideKeyboard();
		// driver.findElement(By.xpath("//android.widget.TextView[@text='Yes, I stay
		// here']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Self-owned']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='With Family']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
		time.pageWaitForTime(8);
	}
}
