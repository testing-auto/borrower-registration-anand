package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class SocialSignUp {
	WaitForTime time = new WaitForTime();
	
	public void page001SocialSignUp() {
		System.out.println("Project : Borrower App - Registration - Happy Path");
		System.out.println("I am inside the Test Execution Process");
		System.out.println("Platform Name: " + BorrowerRegistration.driver.getCapabilities().getCapability("platformName"));
		System.out.println("Platform Version : " + BorrowerRegistration.driver.getCapabilities().getCapability("platformVersion"));
		System.out.println("Device Name : " + BorrowerRegistration.driver.getCapabilities().getCapability("deviceName"));
		System.out.println("Execution Date and Time : " + BorrowerRegistration.driver.getDeviceTime());
		System.out.println("Executed By : Sandeep Singh");
		System.out.println("HP Test Cases");
		time.pageWaitForTime(8);
		try {
			// click Yes I am Agree button
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='YES, I AGREE']")).click();
		} catch (Exception exp) {
			System.out.println("Message is : " + exp.getMessage());
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// click Start Loan application button
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='START LOAN APPLICATION']")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// click login with Mobile
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Continue with Mobile Number']")).click();
		// click Allow button to allow InstaMoney to access your contacts
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@index='0']")).click();
		} catch (Exception ex) {
			ex.getMessage();
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='ALLOW']")).click();
		} catch (Exception ex) {
			ex.getMessage();
		}
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Enter mobile number
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Eg 9903636567']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 1));
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// click Send OTP link
//		new TouchAction(BorrowerRegistration.driver).press(PointOption.point(328, 185)).release().perform()
//		.press(PointOption.point(338, 185)).release().perform();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Send OTP']")).click();
		
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Send OTP']")).click();
		// Enter OTP in text box
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.sendKeys("656539");
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.clear();
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.sendKeys("656590");
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.clear();
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.sendKeys("656511");
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.clear();
		} catch (Exception ex) {

		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sign with gmail
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Sign in with Google']")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Select gmail id
		String email = ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 19);
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='"+email+"']")).click();
		}catch (Exception e) {
			System.out.println("This email is not login with this device, Please login");
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		time.pageScroll("Allow"); //	Scroll down with the help of scroll bar till Allow text
		try {
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[6]/android.widget.Button"))
		.click(); // Click Allow Button
		} catch (Exception e) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='Continue']")).click();
		} catch (Exception e) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='Allow']")).click();
		} catch (Exception e) {
		}
		
		time.pageWaitForTime(2);
	}
	

}
