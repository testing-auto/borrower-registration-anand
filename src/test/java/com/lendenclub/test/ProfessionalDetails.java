package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class ProfessionalDetails {
	WaitForTime time = new WaitForTime();

	public void page002ProfessionalDetails() {

		// Enter Pan Card number
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Enter your pan']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 2));
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 3));
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='dd/mm/yyyy']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 4)); // Enter DOB
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Double Click
		new TouchAction(BorrowerRegistration.driver).press(PointOption.point(328, 185)).release().perform()
		.press(PointOption.point(338, 185)).release().perform();

		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Male']")).click(); // Select Gender
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Salaried']")).click(); // Select Employement type		
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Company name']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 5)); // Enter company name
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Professional Details']")).click();
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Tcm Limited']")).click();
		
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		time.pageScroll("Bank Transfer");  // Scroll bar to scroll till Bank Transfer
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			   
		// Enter Designation
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Designation']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 6));
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
				BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Professional Details']")).click();
		} catch(Exception e)  { }
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Mode of Salary']")).click();
		} catch(Exception e)  { }
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 7)); // Total Work Exp - Year
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 8)); // Total Work Exp - Month
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Mode of Salary']")).click();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 9)); // Current Work Exp - year
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 10)); // Current Work Exp - Month
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Mode of Salary']")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Monthly Salary']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 11)); // Enter Monthly Salary
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Mode of Salary']")).click();
		// Click Mode of Salary
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Bank Transfer']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Click on Preferred Lang drop down box
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Select preferred language']")).click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Select Language
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='ENGLISH']")).click();
//		time.pageWaitForTime(4);
		// Click Save and Continue Button
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SAVE & CONTINUE']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
