package com.lendenclub.test;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class LocationAndCreditDetails {
	WaitForTime time = new WaitForTime();

	public void page003LocationAndCreditDetails() {
		// Click InstaMoney to access this device's location
		BorrowerRegistration.driver
				.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button"))
				.click();
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
					.clear();
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
					.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 12)); // Enter pin code
		} catch (Exception ex) {
			System.out.println("Message : " + ex.getMessage());
		}
		// Select No, i have no loans
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='No, I have no loans']")).click();

		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView"))
				.click(); // Select Yes, I have credit card
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView"))
				.click(); // Select No, I don't have any overdue/unpaid loan
		// Click Bank drop down box
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Select a bank']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='ICICI Bank Ltd']")).click();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[4]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView"))
				.click(); // Select debit card for selected salary account
		// Select Yes, I have netbanking for selected salary account
		time.pageScroll("Yes, I have net banking");
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Yes, I have net banking']")).click();
		time.pageScroll("SUBMIT & CHECK ELIGIBILITY"); // Scroll bar to scroll till SUBMIT & CHECK ELIGIBILITY
		// Click SUBMIT & CHECK ELIGIBILITY button
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT & CHECK ELIGIBILITY']"))
				.click();
		time.pageWaitForTime(2);
	}
}
