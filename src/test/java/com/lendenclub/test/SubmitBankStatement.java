package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class SubmitBankStatement {
	WaitForTime time = new WaitForTime();

	public void page008SubmitBankStatement() {

//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// New changes done by developer
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView"))
		.click(); 
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='5']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Okay']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Search Other Bank']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Search Other Bank']")).sendKeys("icici");
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='ICICI Bank']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='ICICI Bank']")).click();
		
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Select a bank']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='No']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.view.ViewGroup[@index='8']")).click(); // Continue bu
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='YES']")).click();

		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='PROCEED']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='BROWSE']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Okay']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.view.ViewGroup[@index='12']")).click();

		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='7488752498.pdf']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='YES, UPLOAD']")).click();
		time.pageWaitForTime(1);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 22));
		// To minimize the keyboard
		BorrowerRegistration.driver.hideKeyboard();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Okay']")).click();
		// To minimize the keyboard
		BorrowerRegistration.driver.hideKeyboard();
		// driver.findElement(By.xpath("//android.widget.TextView[@text='Of every
		// month']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
		time.pageWaitForTime(3);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		time.pageWaitForTime(60);
		// Confirm bank details
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
			.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 21)); // Account number
		} catch (Exception e) { }
		
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//			} catch (Exception e) { }
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		try {
			BorrowerRegistration.driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
			.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 21)); // Account number
		} catch (Exception e) {	}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 17)); // IFSC code
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		time.pageWaitForTime(1);
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 18)); // Account Holder name
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.hideKeyboard();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
//		time.pageWaitForTime(8);
	}  }
