package com.lendenclub.test.main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import jxl.read.biff.BiffException;
import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;

public class Capabilities {
	public static AndroidDriver<MobileElement> driver;
	// AppiumDriver<MobileElement> driver;
	// private ExcelSheetLibrary excel;
	
	static String appPath = System.getProperty("user.dir")+"/src/test/resources/apps/app-arm64-v8a-release.apk";

	public static AndroidDriver<MobileElement> getDriver() {
		return driver;
	}

	public static void setDriver(AndroidDriver<MobileElement> driver) {
		Capabilities.driver = driver;
	}

	public void setUp() throws BiffException, IOException {

		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
//			caps.setCapability(MobileCapabilityType.VERSION, "11 RP1A.200720.011");
//			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi Note 8 Pro");
//			caps.setCapability(MobileCapabilityType.UDID, "79xsovx4ojbihmfu");
			caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
			
			caps.setCapability(MobileCapabilityType.APP,appPath);
			URL url = new URL("http://localhost:4723/wd/hub");
			driver = new AndroidDriver<MobileElement>(url, caps);
			
			ApkFile apkFile = new ApkFile(new File(appPath));
			ApkMeta apkMeta = apkFile.getApkMeta();
			String versionName = apkMeta.getVersionName();
			String name = apkMeta.getName();
			String versionCode = apkMeta.getVersionCode().toString();
			
			System.out.println("Project : Borrower App - Registration - Happy Path");
			System.out.println("I am inside the Test Execution Process");
			System.out.println("Platform Name: " + BorrowerRegistration.driver.getCapabilities().getCapability("platformName"));
			System.out.println("Platform Version : " + BorrowerRegistration.driver.getCapabilities().getCapability("platformVersion"));
			System.out.println("Device Name : " + BorrowerRegistration.driver.getCapabilities().getCapability("deviceName"));
			System.out.println("The APK name is : " + name);
			System.out.println("The Verion of "+name+" is : " +versionName);
			System.out.println("Execution Date and Time : " + BorrowerRegistration.driver.getDeviceTime());
			System.out.println("Executed By : Flevin");
			System.out.println("HP Test Cases");
			
			ExcelSheetLibrary excel = new ExcelSheetLibrary(
					"./src/test/resources/apps/Borrower_Details.xls");
			
			// TO Clear Source Folder ScreenShot
			File source = new File("./Screenshot");
			FileUtils.cleanDirectory(source);
		} catch (Exception exp) {
			exp.printStackTrace();
		}

	}

}
