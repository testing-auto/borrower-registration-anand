package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class LiveKyc {
	WaitForTime time = new WaitForTime();

	public void page006LiveKyc() {

		try {
			BorrowerRegistration.driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button"))
			.click();
		} catch (Exception ex) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@index='0']"))
			.click();
		} catch (Exception ex) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='WHILE USING THE APP']")).click();
		} catch (Exception e) {
		}
		
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='WHILE USING THE APP']")).click();
		} catch (Exception e) {
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='Allow']")).click();
		} catch (Exception e) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@index='0']"))
			.click();
		} catch (Exception ex) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button"))
			.click();

		} catch (Exception e) {
		}

		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@text='ALLOW']")).click();
		} catch (Exception e) {
		}
		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.Button[@index='0']")).click();
			} catch (Exception e) {
			}
		// Proceed to Liveness Check
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='PROCEED TO LIVENESS CHECK']")).click();
		//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Proceed to Take Selfie']")).click();
		time.pageWaitForTime(40);
	}
}
